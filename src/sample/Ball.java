package sample;

import com.sun.prism.Graphics;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

class Ball {
    float x = random(480);
    float y = random(480);
    float radius = random(20);
    float speedX = random(480);
    float speedY = random(480);
    Color color;
    int width;
    int height;

    public int random(int maxRange) {
        return (int) Math.round(Math.random() * maxRange);
    }

    private final static Color DEFAULT_COLOR = Color.BLACK;


    public Ball(float x, float y, float radius, float speed, float angle, Color color) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.speedX = (float) (speed * Math.cos(Math.toRadians(angle)));
        this.speedY = -speed * (float) Math.sin(Math.toRadians(angle));


    }


    //public Ball(float x, float y, float radius, float speed, float angle) {
      //  this(x, y, radius, speed, angle, DEFAULT_COLOR);
   // }

    void update(Box box) {
        // Get the ball's bounds, offset by the radius of the ball
        float ballMinX = (float) box.minX + radius;
        float ballMinY = (float) box.minY + radius;
        float ballMaxX = (float) box.maxX - radius;
        float ballMaxY = (float) box.maxY - radius;

        x += speedX;
        y += speedY;

        if (x - radius < 0) {

            speedX = -speedX;
            x = radius;
        } else if (x + radius > 500) {

            speedX = -speedX;
            x = 500 - radius;
        }

        if (y - radius < 0) {

            speedY = -speedY;
            y = radius;
        } else if (y + radius > 500) {

            speedY = -speedY;
            y = 500 - radius;
        }
    }

    void draw(GraphicsContext context) {
        context.setFill(color);
        context.fillOval(x, y, radius, radius);
    }
}